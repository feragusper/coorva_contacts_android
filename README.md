# Coorva Contacts

A Simple Android Application that displays a list of Contacts provided by Coorva.

Support
-----------------
If you've found an error in this project, please contact feragusper@gmail.com

Contribute
-----------------
Pull requests are welcome.

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

Build Configuration
-----------------
This project was build on JDK 1.8

Requirements
-----------------
- [Android SDK](http://developer.android.com/sdk/index.html). 

Libraries and tools included
-----------------
- Support Libraries
- RecyclerViews 
- [OkHTTP](http://square.github.io/okhttp/)
- [Gson](https://github.com/google/gson/)
- [Stetho](http://facebook.github.io/stetho/)
- [Dagger 2](http://google.github.io/dagger/)
- [Glide](https://github.com/bumptech/glide/)