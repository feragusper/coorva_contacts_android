package com.coorva.coorvacontacts.core.platform

import android.app.Application
import com.coorva.coorvacontacts.BuildConfig
import com.coorva.coorvacontacts.core.di.ApplicationComponent
import com.coorva.coorvacontacts.core.di.ApplicationModule
import com.coorva.coorvacontacts.core.di.DaggerApplicationComponent
import com.facebook.stetho.Stetho
import com.squareup.leakcanary.LeakCanary
import okhttp3.OkHttpClient
import javax.inject.Inject

/**
 * @author Fernando Perez
 * @since 0.1
 */
class AndroidApplication : Application() {

    @Inject
    lateinit var okHttpClient: OkHttpClient

    companion object {
        lateinit var INSTANCE: AndroidApplication
    }

    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        this.injectMembers()

        if (BuildConfig.DEBUG) LeakCanary.install(this)

        if (BuildConfig.DEBUG) Stetho.initializeWithDefaults(this)

        INSTANCE = this
    }

    private fun injectMembers() = appComponent.inject(this)
}
