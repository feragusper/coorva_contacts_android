package com.coorva.coorvacontacts.core.platform

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.coorva.coorvacontacts.core.exception.Failure

/**
 * @author Fernando Perez
 * @since 0.1
 *
 * Base ViewModel class with default Failure handling.
 * @see ViewModel
 * @see Failure
 */
abstract class BaseViewModel : ViewModel() {

    var failure: MutableLiveData<Failure> = MutableLiveData()

    fun handleFailure(failure: Failure) {
        this.failure.value = failure
    }
}