package com.coorva.coorvacontacts.core.platform

import android.content.Context
import com.coorva.coorvacontacts.core.extension.networkInfo
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Fernando Perez
 * @since 0.1
 *
 * Injectable class which returns information about the network connection state.
 */
@Singleton
class NetworkHandler
@Inject constructor(private val context: Context) {
    @Suppress("DEPRECATION")
    val isConnected
        get() = context.networkInfo?.isConnectedOrConnecting
}