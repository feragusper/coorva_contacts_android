package com.coorva.coorvacontacts.core.platform

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.coorva.coorvacontacts.R
import com.coorva.coorvacontacts.core.di.ApplicationComponent
import com.coorva.coorvacontacts.core.extension.appContext
import com.coorva.coorvacontacts.core.extension.viewContainer
import com.coorva.coorvacontacts.core.navigation.Navigator
import io.github.tonnyl.light.Light
import kotlinx.android.synthetic.main.view_progress.*
import javax.inject.Inject

/**
 * @author Fernando Perez
 * @since 0.1
 *
 * Base Fragment class with helper methods for handling views and back button events.
 *
 * @see Fragment
 */
abstract class BaseFragment : Fragment() {

    abstract fun layoutId(): Int

    @Inject
    lateinit var navigator: Navigator

    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (activity?.application as AndroidApplication).appComponent
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(layoutId(), container, false)

    internal fun showProgress() = progressStatus(View.VISIBLE)

    internal fun hideProgress() = progressStatus(View.GONE)

    private fun progressStatus(viewStatus: Int) =
            with(activity) { if (this is BaseActivity) this.progress.visibility = viewStatus }

    internal fun notifyWithAction(@StringRes message: Int, @StringRes actionText: Int, action: () -> Any) {
        val snackBar = Light.error(viewContainer, resources.getString(message), Snackbar.LENGTH_INDEFINITE)

        snackBar.setAction(actionText) { _ -> action.invoke() }
        snackBar.setActionTextColor(ContextCompat.getColor(appContext,
                R.color.colorTextPrimary))
        snackBar.show()
    }

    internal fun notify(@StringRes message: Int) = Light.error(viewContainer, resources.getString(message), Snackbar.LENGTH_SHORT).show()
}
