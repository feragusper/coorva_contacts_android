package com.coorva.coorvacontacts.core.di.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.coorva.coorvacontacts.feature.contacts.viewmodel.ContactDetailViewModel
import com.coorva.coorvacontacts.feature.contacts.viewmodel.ContactListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 *
 * @author Fernando Perez
 * @since 0.1
 */
@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ContactListViewModel::class)
    abstract fun bindsContactListViewModel(contactListViewModel: ContactListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ContactDetailViewModel::class)
    abstract fun bindsContactDetailViewModel(contactDetailViewModel: ContactDetailViewModel): ViewModel
}