package com.coorva.coorvacontacts.core.di

import com.coorva.coorvacontacts.core.di.viewmodel.ViewModelModule
import com.coorva.coorvacontacts.core.platform.AndroidApplication
import com.coorva.coorvacontacts.feature.contacts.fragment.ContactDetailFragment
import com.coorva.coorvacontacts.feature.contacts.fragment.ContactListFragment
import dagger.Component
import javax.inject.Singleton

/**
 * @author Fernando Perez
 * @since 0.1
 */
@Singleton
@Component(modules = [ApplicationModule::class, ViewModelModule::class])
interface ApplicationComponent {
    fun inject(application: AndroidApplication)
    fun inject(application: ContactListFragment)
    fun inject(contactDetailFragment: ContactDetailFragment)
}
