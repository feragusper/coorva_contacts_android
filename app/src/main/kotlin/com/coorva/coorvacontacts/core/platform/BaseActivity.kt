package com.coorva.coorvacontacts.core.platform

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.coorva.coorvacontacts.R
import com.coorva.coorvacontacts.core.di.ApplicationComponent
import com.coorva.coorvacontacts.core.extension.inTransaction
import kotlinx.android.synthetic.main.toolbar.*

/**
 * @author Fernando Perez
 * @since 0.1
 *
 * Base Activity class with helper methods for handling fragment transactions and back button
 * events.
 *
 * @see AppCompatActivity
 */
abstract class BaseActivity : AppCompatActivity() {

    protected open val layoutResID: Int
        get() = R.layout.activity_layout

    protected open val toolbarTitle: CharSequence
        get() = getText(R.string.app_name)

    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (application as AndroidApplication).appComponent
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResID)

        initializeToolbar(toolbar)

        addFragment(savedInstanceState)
    }

    protected open fun initializeToolbar(toolbar: Toolbar?) {
        setSupportActionBar(toolbar)

        supportActionBar!!.title = toolbarTitle
    }

    protected fun enableUpNavigation() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var onOptionsItemSelected = true
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> onOptionsItemSelected = super.onOptionsItemSelected(item)
        }
        return onOptionsItemSelected
    }

    private fun addFragment(savedInstanceState: Bundle?) =
            savedInstanceState ?: supportFragmentManager.inTransaction {
                add(
                        R.id.fragmentContainer, fragment())
            }

    abstract fun fragment(): BaseFragment
}
