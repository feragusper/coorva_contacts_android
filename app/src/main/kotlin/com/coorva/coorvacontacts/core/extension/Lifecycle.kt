package com.coorva.coorvacontacts.core.extension

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import com.coorva.coorvacontacts.core.exception.Failure

fun <T : Any, L : LiveData<T>> LifecycleOwner.observeSuccess(liveData: L, body: (T?) -> Unit) =
        liveData.observe(this, Observer(body))

fun <L : LiveData<Failure>> LifecycleOwner.observeFailure(liveData: L, body: (Failure?) -> Unit) =
        liveData.observe(this, Observer(body))