package com.coorva.coorvacontacts.core.navigation

import android.support.v4.app.FragmentActivity
import android.view.View
import com.coorva.coorvacontacts.feature.contacts.activity.ContactDetailActivity
import com.coorva.coorvacontacts.feature.contacts.view.ContactView
import javax.inject.Inject
import javax.inject.Singleton


/**
 * @author Fernando Perez
 * @since 0.1
 */
@Singleton
class Navigator
@Inject constructor() {
    @Suppress("UNUSED_PARAMETER")
    fun showContactDetails(activity: FragmentActivity, contactView: ContactView) {
        activity.startActivity(ContactDetailActivity.callingIntent(activity, contactView))
    }

    class Extras(val transitionSharedElement: View)
}


