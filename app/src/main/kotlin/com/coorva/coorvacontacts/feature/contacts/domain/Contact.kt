package com.coorva.coorvacontacts.feature.contacts.domain

/**
 * @author Fernando Perez
 * @since 0.1
 */
data class Contact(val id: String?,
                   val firstName: String?,
                   val lastName: String?,
                   val email: String?,
                   val phone: String?,
                   val image_uri: String?)