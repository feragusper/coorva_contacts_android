package com.coorva.coorvacontacts.feature.contacts.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.coorva.coorvacontacts.core.platform.BaseActivity
import com.coorva.coorvacontacts.feature.contacts.fragment.ContactDetailFragment
import com.coorva.coorvacontacts.feature.contacts.view.ContactView

/**
 * @author Fernando Perez
 * @since 0.2
 */
class ContactDetailActivity : BaseActivity() {

    companion object {
        private const val INTENT_EXTRA_PARAM_CONTACT = "com.coorva.INTENT_PARAM_CONTACT"

        fun callingIntent(context: Context, contact: ContactView): Intent {
            val intent = Intent(context, ContactDetailActivity::class.java)
            intent.putExtra(INTENT_EXTRA_PARAM_CONTACT, contact)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        enableUpNavigation()
    }

    override fun fragment() = ContactDetailFragment.forContact(intent.getParcelableExtra(INTENT_EXTRA_PARAM_CONTACT))
}
