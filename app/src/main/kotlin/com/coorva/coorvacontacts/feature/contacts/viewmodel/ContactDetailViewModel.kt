package com.coorva.coorvacontacts.feature.contacts.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.coorva.coorvacontacts.core.platform.BaseViewModel
import com.coorva.coorvacontacts.feature.contacts.domain.Contact
import com.coorva.coorvacontacts.feature.contacts.usecase.GetContactDetailUseCase
import com.coorva.coorvacontacts.feature.contacts.view.ContactDetailView
import javax.inject.Inject

/**
 * @author Fernando Perez
 * @since 0.2
 */
class ContactDetailViewModel
@Inject constructor(private val getContactDetailUseCase: GetContactDetailUseCase) : BaseViewModel() {

    var contactDetail: MutableLiveData<ContactDetailView> = MutableLiveData()

    fun loadContactDetail(id: String) =
            getContactDetailUseCase(GetContactDetailUseCase.Params(id)) { it.either(::handleFailure, ::handleContactDetail) }

    private fun handleContactDetail(contact: Contact) {
        this.contactDetail.value = ContactDetailView(contact.id, contact.firstName, contact.lastName, contact.email, contact.phone, contact.image_uri)
    }
}