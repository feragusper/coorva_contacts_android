package com.coorva.coorvacontacts.feature.contacts.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.coorva.coorvacontacts.R
import com.coorva.coorvacontacts.core.extension.inflate
import com.coorva.coorvacontacts.core.extension.loadFromUrl
import com.coorva.coorvacontacts.feature.contacts.view.ContactView
import kotlinx.android.synthetic.main.row_contact.view.*
import javax.inject.Inject
import kotlin.properties.Delegates

/**
 * @author Fernando Perez
 * @since 0.1
 */
class ContactListAdapter
@Inject constructor() : RecyclerView.Adapter<ContactListAdapter.ViewHolder>() {

    internal var collection: List<ContactView> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    internal var clickListener: (ContactView) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(R.layout.row_contact))

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) =
            viewHolder.bind(collection[position], clickListener)

    override fun getItemCount() = collection.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(contactView: ContactView, clickListener: (ContactView) -> Unit) {
            itemView.image.loadFromUrl(contactView.imageUri)
            itemView.full_name.text = contactView.fullName
            itemView.setOnClickListener { clickListener(contactView) }
        }
    }
}
