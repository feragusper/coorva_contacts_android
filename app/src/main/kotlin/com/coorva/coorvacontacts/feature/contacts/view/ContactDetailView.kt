package com.coorva.coorvacontacts.feature.contacts.view

data class ContactDetailView(val id: String?,
                             val firstName: String?,
                             val lastName: String?,
                             val email: String?,
                             val phone: String?,
                             val imageUri: String?) {
    val fullName: String
        get() = "$firstName $lastName"
}
