package com.coorva.coorvacontacts.feature.contacts.activity

import com.coorva.coorvacontacts.R
import com.coorva.coorvacontacts.core.platform.BaseActivity
import com.coorva.coorvacontacts.feature.contacts.fragment.ContactListFragment

/**
 * @author Fernando Perez
 * @since 0.1
 */
class ContactListActivity : BaseActivity() {

    override val toolbarTitle: CharSequence
        get() = resources.getString(R.string.contacts)

    override fun fragment() = ContactListFragment()
}
