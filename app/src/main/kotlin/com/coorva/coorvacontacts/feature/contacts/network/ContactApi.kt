package com.coorva.coorvacontacts.feature.contacts.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Fernando Perez
 * @since 0.1
 */
internal interface ContactApi {
    companion object {
        private const val PARAM_CONTACT_ID = "id"
        private const val CONTACT_LIST_ENDPOINT = "contacts/"
        private const val CONTACT_DETAIL_ENDPOINT = "contacts/detail"
    }

    @GET(CONTACT_LIST_ENDPOINT)
    fun contactList(): Call<List<ContactEntity>>

    @GET(CONTACT_DETAIL_ENDPOINT)
    fun contactDetail(@Query(PARAM_CONTACT_ID) id: String): Call<ContactEntity>
}
