@file:Suppress("EXPERIMENTAL_FEATURE_WARNING")

package com.coorva.coorvacontacts.feature.contacts.usecase

import com.coorva.coorvacontacts.core.interactor.UseCase
import com.coorva.coorvacontacts.feature.contacts.domain.Contact
import com.coorva.coorvacontacts.feature.contacts.repository.ContactRepository
import javax.inject.Inject

/**
 * @author Fernando Perez
 * @since 0.2
 */
class GetContactDetailUseCase
@Inject constructor(private val contactRepository: ContactRepository) : UseCase<Contact, GetContactDetailUseCase.Params>() {

    override suspend fun run(params: Params) = contactRepository.contactDetail(params.id)

    data class Params(val id: String)
}
