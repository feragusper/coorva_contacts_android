package com.coorva.coorvacontacts.feature.contacts.fragment

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.coorva.coorvacontacts.R
import com.coorva.coorvacontacts.core.exception.Failure
import com.coorva.coorvacontacts.core.extension.*
import com.coorva.coorvacontacts.core.platform.BaseFragment
import com.coorva.coorvacontacts.feature.contacts.adapter.ContactListAdapter
import com.coorva.coorvacontacts.feature.contacts.view.ContactView
import com.coorva.coorvacontacts.feature.contacts.viewmodel.ContactListViewModel
import kotlinx.android.synthetic.main.fragment_contact_list.*
import kotlinx.android.synthetic.main.view_empty_list.*
import javax.inject.Inject


/**
 * @author Fernando Perez
 * @since 0.1
 */
class ContactListFragment : BaseFragment() {

    @Inject
    lateinit var contactListAdapter: ContactListAdapter

    private lateinit var contactListViewModel: ContactListViewModel

    override fun layoutId() = R.layout.fragment_contact_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        contactListViewModel = viewModel(viewModelFactory) {
            observeSuccess(contactList, ::renderContactList)
            observeFailure(failure, ::handleFailureInView)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeView()
        loadContactList()
    }

    private fun initializeView() {
        contact_list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        contact_list.adapter = contactListAdapter
        contactListAdapter.clickListener = { contact ->
            navigator.showContactDetails(activity!!, contact)
        }

        swipe_refresh_layout.setOnRefreshListener {
            contactListViewModel.loadContactList()
        }
    }

    private fun loadContactList() {
        empty_view.invisible()
        contact_list.visible()
        showProgress()
        contactListViewModel.loadContactList()
    }

    private fun renderContactList(contactList: List<ContactView>?) {
        contactListAdapter.collection = contactList.orEmpty()
        hideProgress()
        swipe_refresh_layout.isRefreshing = false
    }

    private fun handleFailureInView(failure: Failure?) {
        when (failure) {
            is Failure.NetworkConnection -> renderFailure(R.string.failure_network_connection)
            is Failure.ServerError -> renderFailure(R.string.failure_server_error)
        }
    }

    private fun renderFailure(@StringRes message: Int) {
        contact_list.invisible()
        empty_view.visible()
        swipe_refresh_layout.isRefreshing = false
        hideProgress()
        notifyWithAction(message, R.string.retry, ::loadContactList)
    }
}
