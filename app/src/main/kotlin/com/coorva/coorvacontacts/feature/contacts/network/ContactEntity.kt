package com.coorva.coorvacontacts.feature.contacts.network

import com.coorva.coorvacontacts.feature.contacts.domain.Contact

/**
 * @author Fernando Perez
 * @since 0.1
 */
data class ContactEntity(private val id: String? = null,
                         private val first_name: String? = null,
                         private val last_name: String? = null,
                         private val email: String? = null,
                         private val phone: String? = null,
                         private val image: String? = null) {
    fun toContact() = Contact(id, first_name, last_name, email, phone, image)
}
