package com.coorva.coorvacontacts.feature.contacts.fragment

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import com.coorva.coorvacontacts.R
import com.coorva.coorvacontacts.core.exception.Failure
import com.coorva.coorvacontacts.core.extension.*
import com.coorva.coorvacontacts.core.platform.BaseFragment
import com.coorva.coorvacontacts.feature.contacts.view.ContactDetailView
import com.coorva.coorvacontacts.feature.contacts.view.ContactView
import com.coorva.coorvacontacts.feature.contacts.viewmodel.ContactDetailViewModel
import kotlinx.android.synthetic.main.fragment_contact_detail.*
import kotlinx.android.synthetic.main.toolbar.*


/**
 * @author Fernando Perez
 * @since 0.2
 */
class ContactDetailFragment : BaseFragment() {

    companion object {
        private const val PARAM_CONTACT = "param_contact"

        fun forContact(movie: ContactView): ContactDetailFragment {
            val contactDetailFragment = ContactDetailFragment()
            val arguments = Bundle()
            arguments.putParcelable(PARAM_CONTACT, movie)
            contactDetailFragment.arguments = arguments

            return contactDetailFragment
        }
    }

    private lateinit var contactDetailViewModel: ContactDetailViewModel
    private var contactDetailView: ContactDetailView? = null

    override fun layoutId() = R.layout.fragment_contact_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        contactDetailViewModel = viewModel(viewModelFactory) {
            observeSuccess(contactDetail, ::renderContactDetail)
            observeFailure(failure, ::handleFailureInView)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showProgress()
        contactDetailViewModel.loadContactDetail((arguments?.get(PARAM_CONTACT) as ContactView).id!!)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.contact_detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_share -> {
            shareContact()
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    private fun shareContact() {
        val shareBody = resources.getString(R.string.contact_share_template, contactDetailView?.firstName, contactDetailView?.lastName, contactDetailView?.email, contactDetailView?.phone)
        val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, resources.getString(R.string.subject))
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody)
        startActivity(sharingIntent)
    }

    private fun renderContactDetail(contactDetailView: ContactDetailView?) {
        this.contactDetailView = contactDetailView
        contactDetailView?.let { it ->
            with(contactDetailView) {
                activity?.let {
                    image.loadUrlAndPostponeEnterTransition(imageUri, it)
                    it.toolbar.title = fullName
                }
                first_name.text = firstName
                last_name.text = lastName
                email_view.text = email
                phone_view.text = phone
            }
        }
        hideProgress()
    }

    private fun handleFailureInView(failure: Failure?) {
        hideProgress()
        when (failure) {
            is Failure.NetworkConnection -> {
                notify(R.string.failure_network_connection); close()
            }
            is Failure.ServerError -> {
                notify(R.string.failure_server_error); close()
            }
        }
    }
}
