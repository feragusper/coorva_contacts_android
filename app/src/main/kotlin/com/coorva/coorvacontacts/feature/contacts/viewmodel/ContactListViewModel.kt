package com.coorva.coorvacontacts.feature.contacts.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.coorva.coorvacontacts.core.interactor.UseCase
import com.coorva.coorvacontacts.core.platform.BaseViewModel
import com.coorva.coorvacontacts.feature.contacts.domain.Contact
import com.coorva.coorvacontacts.feature.contacts.usecase.GetContactListUseCase
import com.coorva.coorvacontacts.feature.contacts.view.ContactView
import javax.inject.Inject

/**
 * @author Fernando Perez
 * @since 0.1
 */
class ContactListViewModel
@Inject constructor(private val getContactListUseCase: GetContactListUseCase) : BaseViewModel() {

    var contactList: MutableLiveData<List<ContactView>> = MutableLiveData()

    fun loadContactList() = getContactListUseCase(UseCase.None()) { it.either(::handleFailure, ::handleContactList) }

    private fun handleContactList(contactList: List<Contact>) {
        this.contactList.value = contactList.sortedWith(compareBy({ it.firstName }, { it.lastName }, { it.email })).map { ContactView(it.id, it.firstName, it.lastName, it.image_uri) }
    }
}