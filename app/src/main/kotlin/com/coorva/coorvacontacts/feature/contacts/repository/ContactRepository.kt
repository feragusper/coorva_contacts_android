package com.coorva.coorvacontacts.feature.contacts.repository

import com.coorva.coorvacontacts.core.exception.Failure
import com.coorva.coorvacontacts.core.functional.Either
import com.coorva.coorvacontacts.core.platform.NetworkHandler
import com.coorva.coorvacontacts.feature.contacts.domain.Contact
import com.coorva.coorvacontacts.feature.contacts.network.ContactEntity
import com.coorva.coorvacontacts.feature.contacts.service.ContactService
import retrofit2.Call
import javax.inject.Inject

/**
 * @author Fernando Perez
 * @since 0.1
 */
interface ContactRepository {
    fun contactList(): Either<Failure, List<Contact>>
    fun contactDetail(id: String): Either<Failure, Contact>

    class Network
    @Inject constructor(private val networkHandler: NetworkHandler,
                        private val service: ContactService) : ContactRepository {

        override fun contactList(): Either<Failure, List<Contact>> {
            return when (networkHandler.isConnected) {
                true -> request(service.contactList(), { it -> it.map { it.toContact() } }, emptyList())
                false, null -> Either.Left(Failure.NetworkConnection())
            }
        }

        override fun contactDetail(id: String): Either<Failure, Contact> {
            return when (networkHandler.isConnected) {
                true -> request(service.contactDetail(id), { it.toContact() }, ContactEntity())
                false, null -> Either.Left(Failure.NetworkConnection())
            }
        }

        private fun <T, R> request(call: Call<T>, transform: (T) -> R, default: T): Either<Failure, R> {
            return try {
                val response = call.execute()
                when (response.isSuccessful) {
                    true -> Either.Right(transform((response.body() ?: default)))
                    false -> Either.Left(Failure.ServerError())
                }
            } catch (exception: Throwable) {
                Either.Left(Failure.ServerError())
            }
        }
    }
}
