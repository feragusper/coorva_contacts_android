package com.coorva.coorvacontacts.feature.contacts.service

import com.coorva.coorvacontacts.feature.contacts.network.ContactApi
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Fernando Perez
 * @since 0.1
 */
@Singleton
class ContactService
@Inject constructor(retrofit: Retrofit) : ContactApi {
    private val contactApi by lazy { retrofit.create(ContactApi::class.java) }

    override fun contactList() = contactApi.contactList()
    override fun contactDetail(id: String) = contactApi.contactDetail(id)
}
