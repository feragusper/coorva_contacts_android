package com.coorva.coorvacontacts.feature.contacts.view

import android.os.Parcel
import com.coorva.coorvacontacts.core.platform.KParcelable
import com.coorva.coorvacontacts.core.platform.parcelableCreator

data class ContactView(val id: String?,
                       val firstName: String?,
                       val lastName: String?,
                       val imageUri: String?) : KParcelable {

    val fullName: String
        get() = "$firstName $lastName"

    companion object {
        @JvmField
        val CREATOR = parcelableCreator(::ContactView)
    }

    constructor(parcel: Parcel) : this(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString())

    override fun writeToParcel(dest: Parcel, flags: Int) {
        with(dest) {
            writeString(id)
            writeString(firstName)
            writeString(lastName)
            writeString(imageUri)
        }
    }
}
