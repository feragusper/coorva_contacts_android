package com.coorva.coorvacontacts.feature.contacts.usecase

import com.coorva.coorvacontacts.core.interactor.UseCase
import com.coorva.coorvacontacts.feature.contacts.domain.Contact
import com.coorva.coorvacontacts.feature.contacts.repository.ContactRepository
import javax.inject.Inject

/**
 * @author Fernando Perez
 * @since 0.1
 */
@Suppress("EXPERIMENTAL_FEATURE_WARNING")
class GetContactListUseCase
@Inject constructor(private val contactRepository: ContactRepository) : UseCase<List<Contact>, UseCase.None>() {

    override suspend fun run(params: None) = contactRepository.contactList()
}
